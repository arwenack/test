/** 
 * Using a public api or local json files, get data from 2 or more endpoints and then merge and filter the results.
 * 
 * Please provide the full solution so that we can run it locally.
 * 
 * Use vanilla JavaScript or a framework - we are particularly interested in seeing use of the new methods in ES6.
 * 
 */
import "isomorphic-fetch"

const api = `https://dog.ceo/api/breeds/list/all`;

// get first list of doggies
const promise0 = fetch(api)
  .then(response => response.json())
  .catch(err => console.error(err));

// get second list of doggies
const promise1 = fetch(api, { method: "GET" })
  .then(response => response.json())
  .catch(err => console.error(err));

// execute above promises
Promise.all([promise1, promise0])
  .then(values => {
    // merge all the doggies
    const doggies = [...Object.keys(values[0].message), ...Object.keys(values[1].message)];

    // de-dup
    const deduped = doggies.filter((el, i, arr) =>
      arr.indexOf(el) === i
    );

    // fin!
    console.log('deduped doggies!', deduped);
  })
  .catch(err => console.error(err));
