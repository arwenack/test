# README #

$ npm install -g babel-cli
$ git clone git@bitbucket.org:arwenack/test.git
$ cd test
$ npm i
$ npm start

### Task ###

Using a public api or local json files, get data from 2 or more endpoints and then merge and filter the results.

Please provide the full solution so that we can run it locally.

Use vanilla JavaScript or a framework - we are particularly interested in seeing use of the new methods in ES6.

